
DLP Protector:

Protector Prerequisites Role: protector-installation

Following task will be performed by the playbook

1. check protector already installed or not
2. Download protector installable file from Artifactory
3. install the protector application
4. install and configure snmp and chrony 